# syntax=docker/dockerfile:1

FROM gentoo/portage:latest AS portage

FROM gentoo/stage3:latest

COPY --from=portage /var/db/repos/gentoo /var/db/repos/gentoo
